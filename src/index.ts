//@ts-nocheck
import { OfficeDocumentTypes, OfficeDocument } from "./OfficeDocument"
import { SpreadsheetDocument } from "./SpreadsheetDocument"
import { TextDocument } from "./TextDocument"
import * as Color from "color";

/**
 * This will create a new {@link OfficeDocument} with of the chosen type
 * 
 * ## Spreadsheet
 * Creating a new spreadsheet document
 * ```
 * var doc = OfficeDocument.create({ type: "spreadsheet" });
 * var sheet = doc.activeSheet;
 * sheet.setCell("A1", { value: "test" } )
 * ```
 * 
 * ## Text
 * _currently **w**ork **i**n **p**rocess and not available_
 * 
 * ## Text
 * _currently **w**ork **i**n **p**rocess and not available_
 * 
 * ## presentations
 * _currently **w**ork **i**n **p**rocess and not available_
 * 
 * ## graphics
 * _currently **w**ork **i**n **p**rocess and not available_
 * 
 * @param options can be `spreadsheet`, `text`, `presentations` or `graphics` or passed as `{ type: "spreadsheet" }`
 * @returns a new blank {@link OfficeDocument}
 */
function create<T extends keyof OfficeDocumentTypes>(options: T | { type: T }): OfficeDocumentTypes[T] {
    if (typeof(options) == "string") {
        options = { type: options }
    }
    switch (options.type) {
        case "spreadsheet":
            return new SpreadsheetDocument()
        case "text":
            return new TextDocument()
        default:
            throw `${options.type} is not a supported document type`
    }
}

/**@deprecated WIP*/
function load(src: string) {
    throw "not implemented\n"
}

/**
 * # ScriptOffice _for JavaScript_
 * a library to create files of an office software programmcity and automated
 * 
 * ![logo](../logo.svg)
 * 
 * You can beginn using {@link create} or {@link load} to work with office files
 * 
 * @example
 * var doc = OfficeDocument.create({ type: "spreadsheet" });
 * var doc = OfficeDocument.load("test.csv");
 */
export const OfficeDocument =  { create, load }
export { Color }